﻿using Hr_system.Controllers;
using Hr_system.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Hr_system_Test
{
    public class UserContorollerTest
    {
        private UserController _userController;
        private HrContext _context;
        private IWebHostEnvironment _ihost;
        private UserManager<User> _userManager;
        public UserContorollerTest()
        {
            var webhostmocker = new Mock<IWebHostEnvironment>();
            var userManager = new Mock<UserManager<User>>();
            _ihost = webhostmocker.Object;
            _userManager = userManager.Object;
            _userController = new UserController(_ihost, _context, _userManager);

            var options = new DbContextOptionsBuilder<HrContext>()
                                     .UseInMemoryDatabase(databaseName: "test_hr_systemdb")
                                     .Options;
        }
        [Fact]
        public async Task TestCreateUser()
        {


            var options = new DbContextOptionsBuilder<HrContext>()
                              .UseInMemoryDatabase(databaseName: "test_hr_systemdb")
                              .Options;
            using (var context = new HrContext(options))
            {
                int beforeCount = context.Users.Count();
                var user = new User
                {
                    UserName = "vitya",
                    Email = "kiselev@mail.ru",
                    Password = "KiselevVitya29+",
                    SpecialityId = "ekfdlsajkjaf-2312klkew-231sdasd",
                    PretendingId = "dasjdkaskj-23231lkjsad-23jasdsd",
                    MeetTime = DateTime.Now,
                    Link = "google.com/Users/Maquierd23",
                    Rating = 2,
                    Comment = "Слабый",
                    NameOfExamenator = "Виктор",
                    Resume = null


                };
                _userController = new UserController(_ihost, context, _userManager);
                var result = await _userController.CreateUser(user);
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal("Index", redirectToActionResult.ActionName);
                Assert.Equal(beforeCount + 1, context.Users.Count());
            }

        }
        [Fact]
        public async Task CreatePretendent()
        {
            var options = new DbContextOptionsBuilder<HrContext>()
                              .UseInMemoryDatabase(databaseName: "test_hr_systemdb")
                              .Options;
            using (var context = new HrContext(options))
            {
                int beforeCount = context.Users.Count();
                var user = new User
                {
                    UserName = "Checkinson",
                    Email = "Checkinson@mail.ru",
                    Password = "Checkinson29++",
                    SpecialityId = "ekfdlsa23213kjaf-2312klkew-231sdasd",
                    PretendingId = "dasjdkaskj-23231lkjsadadd-23jasdsd",
                    MeetTime = DateTime.Now,
                    Link = "google.com/Users/Checkinson232",
                    Rating = 4,
                    Comment = "Хорош в понимании",
                    NameOfExamenator = "Владимир",
                    Resume = null


                };
                _userController = new UserController(_ihost, context, _userManager);
                var result = await _userController.CreateUser(user);
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal("Index", redirectToActionResult.ActionName);
                Assert.Equal(beforeCount + 1, context.Users.Count());
            }
        }
        [Fact]
        public async Task EditPretendent()
        {
            var options = new DbContextOptionsBuilder<HrContext>()
                              .UseInMemoryDatabase(databaseName: "test_hr_systemdb")
                              .Options;
            using (var context = new HrContext(options))
            {
                var user = new User
                {
                    UserName = "Jason",
                    Email = "jason@glamil.com",
                    Password = "CJasondasdNussan29++",
                    SpecialityId = "ekfdlsa23213kjaf-2312klkew-231sdasd",
                    PretendingId = "dasjdkaskj-23231lkjsadadd-23jasdsd",
                    MeetTime = DateTime.Now,
                    Link = "google.com/Users/Checkinson232",
                    Rating = 4,
                    Comment = "Хорош в понимании",
                    NameOfExamenator = "дИМА",
                    Resume = null
                };

                _userController = new UserController(_ihost, context, _userManager);
                var result = await _userController.Edit(user.ToString());
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal("Index", redirectToActionResult.ActionName);


            }
        }
        [Fact]
        public async Task DeletePretendent()
        {

            var options = new DbContextOptionsBuilder<HrContext>()
                              .UseInMemoryDatabase(databaseName: "test_hr_systemdb")
                              .Options;
            var result = await _userController.Delete("2");
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", redirectToActionResult.ActionName);

        }
        public async Task SearchFilterTest()
        {
            var searchString = "ekfdlsa23213kjaf-2312klkew-231sdasd";
            var currentFilter = "ekfdlsa23213kjaf - 2312klkew - 231sdasd";
                var users = new User[]
            {
                new User{UserName = "Jason",
                    Email = "jason@glamil.com",
                    Password = "CJasondasdNussan29++",
                    SpecialityId = searchString,
                    PretendingId = currentFilter,
                    MeetTime = DateTime.Now,
                    Link = "google.com/Users/Checkinson232",
                    Rating = 4,
                    Comment = "Хорош в понимании",
                    NameOfExamenator = "дИМА",
                    Resume = null},
                new User{UserName = "Jason",
                    Email = "jason@glamil.com",
                    Password = "CJasondasdNussan29++",
                    SpecialityId = currentFilter,
                    PretendingId = searchString,
                    MeetTime = DateTime.Now,
                    Link = "google.com/Users/Checkinson232",
                    Rating = 4,
                    Comment = "Хорош в понимании",
                    NameOfExamenator = "дИМА",
                    Resume = null}
            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            var result = await _userController.Index(searchString, currentFilter, pageNumber) as ViewResult;
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<User>>(viewResult.ViewData.Model);
            Assert.True(model.Where(s => s.Pretending.NameOfPlace == searchString || s.Speciality.Name == searchString).Count() >= 2);
        }
    }
    }

