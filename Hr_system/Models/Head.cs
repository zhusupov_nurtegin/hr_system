﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hr_system.Models
{
    public class Head
    {
        public int Id { get; set; }
        public string Name { get; set; }
       public string Boss { get; set; }
    }
}
