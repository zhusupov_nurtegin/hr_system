﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hr_system.Models
{
    public class Speciality
    {
        public string Id { get; set; }
        public string Name { get; set; }
        
    }
}
