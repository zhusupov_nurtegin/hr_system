﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hr_system.Models
{
    public class User : IdentityUser
    {
        public string SpecialityId { get; set; }
        public Speciality Speciality { get; set; }

        public string PretendingId { get; set; }
        public Pretending Pretending { get; set; }
        public DateTime MeetTime { get; set; }
        public string Link { get; set; }
        [Required(ErrorMessage = "RatingRequired")]
        [Range(0, 5, ErrorMessage = "RatingRange")]
        [Display(Name = "Rating")]
        public int Rating { get; set; }
        public string Comment { get; set; }
        public string NameOfExamenator { get; set; }
        [NotMapped]
        public IFormFile Resume { get; set; }
        public string Password { get; set; }
    }
}
