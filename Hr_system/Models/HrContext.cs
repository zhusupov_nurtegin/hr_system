﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hr_system.Models
{
    public class HrContext : IdentityDbContext<User>
    {
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Pretending> Pretedings { get; set; }
        public DbSet<Head> Heads { get; set; }
        public HrContext(DbContextOptions<HrContext> options)
            : base(options)
        {
        }

    }
}
