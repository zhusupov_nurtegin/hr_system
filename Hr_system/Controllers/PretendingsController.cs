﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hr_system.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hr_system.Controllers
{
   
    [Authorize(Roles = "admin")]

    public class PretendingsController : Controller
    {
        private readonly HrContext _context;

        public PretendingsController(HrContext context)
        {
            _context = context;
        }

        // GET: Admin/Pretendings
        public async Task<IActionResult> Index()
        {
            var hrContext = _context.Pretedings.Include(p => p.Head);
            return View(await hrContext.ToListAsync());
        }

        // GET: Admin/Pretendings/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pretending = await _context.Pretedings
                .Include(p => p.Head)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pretending == null)
            {
                return NotFound();
            }

            return View(pretending);
        }

        // GET: Admin/Pretendings/Create
        [Authorize(Roles = "admin")]
        public IActionResult Create()
        {
            ViewData["HeadId"] = new SelectList(_context.Heads, "Id", "Name");
            return View();
        }

        // POST: Admin/Pretendings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NameOfPlace,HeadId")] Pretending pretending)
        {
            if (ModelState.IsValid)
            {
                pretending.Id = Guid.NewGuid().ToString();
                _context.Add(pretending);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HeadId"] = new SelectList(_context.Heads, "Id", "Name", pretending.HeadId);
            return View(pretending);
        }

        // GET: Admin/Pretendings/Edit/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pretending = await _context.Pretedings.FindAsync(id);
            if (pretending == null)
            {
                return NotFound();
            }
            ViewData["HeadId"] = new SelectList(_context.Heads, "Id", "Id", pretending.HeadId);
            return View(pretending);
        }

        // POST: Admin/Pretendings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,NameOfPlace,HeadId")] Pretending pretending)
        {
            if (id != pretending.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pretending);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PretendingExists(pretending.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HeadId"] = new SelectList(_context.Heads, "Id", "Id", pretending.HeadId);
            return View(pretending);
        }

        // GET: Admin/Pretendings/Delete/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pretending = await _context.Pretedings
                .Include(p => p.Head)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pretending == null)
            {
                return NotFound();
            }

            return View(pretending);
        }

        // POST: Admin/Pretendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var pretending =  _context.Pretedings.Find(id);
            _context.Pretedings.Remove(pretending);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PretendingExists(string id)
        {
            return _context.Pretedings.Any(e => e.Id == id);
        }
    }
}
