﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hr_system.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hr_system.Controllers
{

    [Authorize(Roles = "admin")]
    public class HeadsController : Controller
    {
        private readonly HrContext _context;

        public HeadsController(HrContext context)
        {
            _context = context;
        }

        // GET: Admin/Heads
        public async Task<IActionResult> Index()
        {
            return View(await _context.Heads.ToListAsync());
        }

        // GET: Admin/Heads/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var head = await _context.Heads
                .FirstOrDefaultAsync(m => m.Id == id);
            if (head == null)
            {
                return NotFound();
            }

            return View(head);
        }

        // GET: Admin/Heads/Create
        [Authorize(Roles = "admin")]
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name");
            return View();
        }

        // POST: Admin/Heads/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Boss")] Head head)
        {
            if (ModelState.IsValid)
            {
                _context.Add(head);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(head);
        }

        // GET: Admin/Heads/Edit/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var head = await _context.Heads.FindAsync(id);
            if (head == null)
            {
                return NotFound();
            }
          
            return View(head);
        }

        // POST: Admin/Heads/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Boss")] Head head)
        {
            if (id != head.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(head);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HeadExists(head.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
         
            return View(head);
        }

        // GET: Admin/Heads/Delete/5
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var head = await _context.Heads
                .FirstOrDefaultAsync(m => m.Id == id);
            if (head == null)
            {
                return NotFound();
            }

            return View(head);
        }

        // POST: Admin/Heads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var head = await _context.Heads.FindAsync(id);
            _context.Heads.Remove(head);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HeadExists(int id)
        {
            return _context.Heads.Any(e => e.Id == id);
        }
    }
}
