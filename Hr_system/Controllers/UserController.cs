﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hr_system.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Hr_system.Controllers
{
    
    [Authorize]
    public class UserController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private HrContext _context;
        private UserManager<User> _userManager;
        public UserController(IWebHostEnvironment webHostEnvironment,HrContext context, UserManager<User> usermanager)
        {
            _webHostEnvironment = webHostEnvironment;
            _context = context;
            _userManager = usermanager;
        }
        public async Task<IActionResult> Index(string searchString, string currentFilter, int? pageNumber)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var users = from s in _context.Users
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.UserName.Contains(searchString)
                                       || s.Email.Contains(searchString) ||
                                      s.Pretending.NameOfPlace.Contains(searchString) ||
                                      s.Pretending.Head.Name.Contains(searchString));
            }
            ViewData["SpecialityId"] = (_context.Specialities, "Id", "Name");
            ViewData["PretendingId"] = (_context.Pretedings, "Id", "NameOfPlace");

            int pageSize = 5;
            return View(await PaginatedList<User>.CreateAsync(users.AsNoTracking(), pageNumber ?? 1, pageSize));
        }
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }
        [Authorize(Roles = "admin")]
        public IActionResult CreateUser()
        {
            ViewData["SpecialityId"] = new SelectList(_context.Specialities, "Id", "Name");
            ViewData["PretendingId"] = new SelectList(_context.Pretedings, "Id", "NameOfPlace");
           
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateUser([Bind("Email,UserName,Password,SpecialityId,PretendingId,MeetTime,Link,Rating,Comment," +
            "NameOfExamenator,Resume")]User user)
        {
            if (ModelState.IsValid)
            {
                if (user.Resume != null)
                {
                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "SomeFiles", "Resumes");
                    string fileName = $"{Guid.NewGuid().ToString()}_{user.Resume.FileName}";
                    string filePath = Path.Combine(folderPath, fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        user.Resume.CopyTo(stream);
                    }


                }
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SpecialityId"] = new SelectList(_context.Specialities, "Id", "Name", user.SpecialityId);
            ViewData["PretendingId"] = new SelectList(_context.Pretedings, "Id", "NameOfPlace",user.PretendingId);
            return View(user);
        }
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["PretendingId"] = new SelectList(_context.Pretedings, "Id", "Id", user.PretendingId);
            ViewData["SpecialityId"] = new SelectList(_context.Specialities, "Id", "Id", user.SpecialityId);
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("SpecialityId,PretendingId,MeetTime,Link,Rating,Comment,Resume")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (user.Resume != null)
                    {
                        string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "SomeFiles", "Resumes");
                        string fileName = $"{Guid.NewGuid().ToString()}_{user.Resume.FileName}";
                        string filePath = Path.Combine(folderPath, fileName);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            user.Resume.CopyTo(stream);
                        }

 
                    }
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PretendingId"] = new SelectList(_context.Pretedings, "Id", "Id", user.PretendingId);
            ViewData["SpecialityId"] = new SelectList(_context.Specialities, "Id", "Id", user.SpecialityId);
            return View(user);
        }
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Admin/Phones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Search(string search)
        {
            List<User> users = await _userManager.Users.Where(p => p.Email.Contains(search) || p.UserName.Contains(search)).ToListAsync();
            return View(users);
        }

        private bool UserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
