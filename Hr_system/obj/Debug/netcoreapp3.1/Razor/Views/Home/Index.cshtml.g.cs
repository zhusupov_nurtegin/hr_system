#pragma checksum "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6131df1d45a6ed5a58c5575023187213c1cf9ac3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\_ViewImports.cshtml"
using Hr_system;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\_ViewImports.cshtml"
using Hr_system.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\_ViewImports.cshtml"
using Hr_system.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6131df1d45a6ed5a58c5575023187213c1cf9ac3", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"71f6f9c37995ed6cdd2aed14afe2b9ca8a10be0e", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Hr_system.Models.User>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\Home\Index.cshtml"
 if (User.IsInRole("") || User.IsInRole("Сотрудник"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("<p>Добро пожаловать!</p>\n<p2> Для просмотра информации, выберите нужные вам заголовки.</p2>\n");
#nullable restore
#line 6 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\Home\Index.cshtml"
}

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\Home\Index.cshtml"
 if (User.IsInRole("admin"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <h1> Добро пожаловать, старший администратор!</h1>\r\n");
#nullable restore
#line 10 "C:\Users\ASUS_STRIX\Desktop\Hr_system\Hr_system\Views\Home\Index.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Hr_system.Models.User>> Html { get; private set; }
    }
}
#pragma warning restore 1591
