﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hr_system.Migrations
{
    public partial class ChangeBossToUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Boss",
                table: "Heads");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Heads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Heads_UserId",
                table: "Heads",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Heads_AspNetUsers_UserId",
                table: "Heads",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Heads_AspNetUsers_UserId",
                table: "Heads");

            migrationBuilder.DropIndex(
                name: "IX_Heads_UserId",
                table: "Heads");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Heads");

            migrationBuilder.AddColumn<string>(
                name: "Boss",
                table: "Heads",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
